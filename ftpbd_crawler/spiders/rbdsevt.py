from typing import List
import scrapy


class RbdsevtSpider(scrapy.Spider):
    name = "rbdsevt"
    allowed_domains = ["localhost"]
    start_urls = ["http://localhost:3000/archives"]

    def parse(self, response):
        headings = response.css(".EventCard_card-text__3gt-v a")
        for h in headings:
            yield response.follow(h, callback=self.parse_article)

    def parse_article(self, response):
        heading = response.css(".MainEvent_post-title__3xF6O::text").get()
        time = response.css(".MainEvent_meta__31Nsd time::text").get()
        text: List[str] = response.css(
            ".MainEvent_main-event__34s8p article ::text"
        ).getall()

        yield {
            "heading": heading,
            "time": time,
            "text": "".join(text),
        }
