import scrapy

is_folder = lambda res: res.css(".fb-i > img::attr(alt)").get() == "folder"


class TvSpider(scrapy.Spider):
    name = "tv"
    allowed_domains = ["server4.ftpbd.net"]
    start_urls = [
        "http://server4.ftpbd.net/FTP-4/English & Foreign TV Series/Parks and Recreation (TV Series 2009-2015) 720p/"
    ]

    def parse(self, response):
        dir = response.css("table tr")
        for res in dir:
            name: str = res.css(".fb-n a::text").get()
            href: str = res.css(".fb-n > a::attr(href)").get()
            if href != None and "/" in href:
                if is_folder(res):
                    yield response.follow(href, callback=self.parse)
                else:
                    yield {"name": name, "link": href}
