import scrapy

suffixes = (
    ".mp4",
    ".mkv",
    ".avi",
    ".jpg",
    ".png",
    ".bmp",
    ".m4a",
    ".m4v",
    "mov",
    "3gp",
    "3gpp",
    "ogg",
    "wmv",
    "webm",
    "flv",
    "ts",
    "aac",
    "gif",
    "mp3",
    "jpeg",
    "srt",
    "ssa",
    "ass",
    "db",
    "zip",
    "rar",
    "idx",
)


is_folder = lambda res: res.css(".fb-i > img::attr(alt)").get() == "folder"


class MoviesSpider(scrapy.Spider):
    name = "movies"
    allowed_domains = ["server2.ftpbd.net"]
    start_urls = [
        # "http://server2.ftpbd.net/FTP-2/English%20Movies/",
        "http://server2.ftpbd.net/FTP-2/English%20Movies/IMDB%20TOP%20250/",
    ]

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse)

    def parse(self, response):
        dir = response.css("table tr")
        for res in dir:
            name: str = res.css(".fb-n a::text").get()
            href: str = res.css(".fb-n > a::attr(href)").get()
            if href != None and "/" in href:
                if is_folder(res):
                    yield response.follow(href, callback=self.parse)
                else:
                    yield {"name": name, "link": href}
